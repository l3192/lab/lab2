from typing import Optional

from fastapi import FastAPI, Form
from pydantic import BaseModel

class TestRequest(BaseModel):
    name: str
    description: Optional[str] = None

app = FastAPI()

@app.get("/{id}")
def read_item(id: int, query: Optional[str] = None):
  if query is None:
    query = "No query provided"
  return {
    "id": id,
    "query": query
  }

@app.post("/form-url-encoded")
def post_form_url_encoded(query_param: str, key: str = Form(...)):
  return {
    "query_param": query_param,
    "key": key 
  }

@app.post("/application-json")
def post_application_json(query_param: str, body: TestRequest):
  return {
    "query_param": query_param,
    "body_name": body.name,
    "body_description": body.description
  }